#ifndef __OBJ_H
#define __OBJ_H

#include <Angel.h>


typedef Angel::vec4  color4;
typedef Angel::vec4  point4;
typedef Angel::vec3  point3;


// Class that describes a objects state.
// Genereates the model_view matrix.
class GLObjectState {
    protected:
        // Initial state variables for recovery.
        GLfloat init_x;
        GLfloat init_z;
        GLfloat init_scale;

        // State variables to generate ModelViewMatrix
        mat4 rotateMatrix;
        GLfloat scale;
    public:
        static const GLfloat DEFAULT_X;
        static const GLfloat DEFAULT_Z;
        static const GLfloat DEFAULT_SCALE;
        static const GLfloat MIN_SCALE;
        static const GLfloat MAX_SCALE;

        // Explicit constructor.
        GLObjectState(
            const GLfloat x_rot = DEFAULT_X, 
            const GLfloat z_rot = DEFAULT_Z,
            const GLfloat scale = DEFAULT_SCALE);
        
        // rotation methods
        void rotateX(const GLfloat rot);
        void rotateZ(const GLfloat rot);

        // zoom methods
        void zoomIn(const GLfloat val);
        void zoomOut(const GLfloat val);

        // reset methods
        void resetRotation();
        void reset();

        mat4 getModelViewMatrix();
};


// abstract GLObject used for abstracting the details of
// each type of object to draw
class GLObject {
    protected:
        static const char* VERTEX_POSITION_ATTRIB; // Vertex position attribute name
        static const char* VERTEX_COLOR_ATTRIB; // Vertex color attribute name
        static const char* MODEL_MATRIX_ATTRIB; // Model matrix attrib name.

        GLuint program; // program object belongs to.
    public:
        GLObject(GLuint program);
        virtual void allocate() =0; // method for allocating memory in gpu
        virtual void draw() =0; // method for drawing the pre-allocated object
};

// Simple cube class for ease of testing.
class GLCube : public GLObject {
    protected:
        GLuint vao_id; // vertex array object for storing vertex data.
        GLuint vbo_ids[2]; // vertex buffer objects. index 0 for vertex data, index 1 for index data.
        GLuint positionAttrib; // position attrib location
        GLuint colorAttrib; // color attrib location

        // Vertices
        point4 vertices[8] = {
            point4(-.5f, -.5f,  .5f, 1),
            point4(-.5f,  .5f,  .5f, 1),
            point4( .5f,  .5f,  .5f, 1),
            point4( .5f, -.5f,  .5f, 1),
            point4(-.5f, -.5f, -.5f, 1),
            point4(-.5f,  .5f, -.5f, 1),
            point4( .5f,  .5f, -.5f, 1),
            point4( .5f, -.5f, -.5f, 1),
        };

        // Indices
        GLuint indices[36] = {
            0,2,1,  0,3,2,
            4,3,0,  4,7,3,
            4,1,5,  4,0,1,
            3,6,2,  3,7,6,
            1,6,5,  1,2,6,
            7,5,6,  7,4,5
        };

    public:
        GLCube(GLuint program);
        void allocate();
        void draw();
};

/* 
    A cube with colors on each face.
*/
class GLColorCube : public GLObject {
    protected:
        static const color4 DEFAULT_FACE_COLORS[6];

        GLuint vao_id; // vertex array object for storing vertex data.
        GLuint vbo_id; // vertex buffer object for storing vertices and colors.
        GLuint positionAttrib; // position attrib location
        GLuint colorAttrib; // color attrib location

        // Vertices
        point4 vertices[8] = {
            point4(-.5f, -.5f,  .5f, 1),
            point4(-.5f,  .5f,  .5f, 1),
            point4( .5f,  .5f,  .5f, 1),
            point4( .5f, -.5f,  .5f, 1),
            point4(-.5f, -.5f, -.5f, 1),
            point4(-.5f,  .5f, -.5f, 1),
            point4( .5f,  .5f, -.5f, 1),
            point4( .5f, -.5f, -.5f, 1),
        };

        // One color for each face.
        color4 faceColors[6];

        GLuint indices[36] = {
            0,2,1,  0,3,2,
            4,3,0,  4,7,3,
            4,1,5,  4,0,1,
            3,6,2,  3,7,6,
            1,6,5,  1,2,6,
            7,5,6,  7,4,5
        };

    public:
        // Extra param colors[6], one color for each face.
        GLColorCube(GLuint program, color4 colors[6]);
        void allocate();
        void draw();
};

/*
    A color cube that has few extra methods for used for picking.
*/
class GLSelectionCube : public GLColorCube {
    protected:
        // Additionally this object has its own modelMatrix.
        // Because we want it to be drawable by itself.
        GLuint modelMatrixLocation;
        mat4 modelMatrix;

    public:
        using GLColorCube::GLColorCube;
        GLSelectionCube(GLuint program);
        void draw();
        int select(int x, int y); // Returns selected face.

};

/* A rubicks cube, consisting of 26 colored cubes */
class GLRubiksCube : public GLObject {
    public:
        enum direction_t {CW, CCW}; // Rotation directions of a face.

    protected:
        // Rubiks cube constists of 26 ordinary cubes.
        static const int NUM_SUBCUBES = 26;

        // Scale of a subCube to whole rubiks cube.
        float scaleSize = 0.31;
        float scaleDistance = 0.33;

        GLuint modelMatrixLocation;
        GLColorCube *subCubes[NUM_SUBCUBES];

        // A two dimentional array to hold indices of the subcubes for each face.
        int faces[6][9];


        int rotatingFace = -1; // Currently rotating face. -1 means no face.
        float rotatedAngle = 0.0; // How much the face rotated.

        // Positions of cubes relative to center of rubik cube
        point3 subCubePositions[NUM_SUBCUBES];

        // There are 26 matricies used for positioning and moving each subcube.
        mat4 subCubeModelMatrices[NUM_SUBCUBES];

        // Method used for finishing a face rotation.
        void completeRotation(int faceNum, direction_t direction);

        // Method used for readying a face rotation.
        bool startRotation(int faceNum);

        // Methos used for rotating a face around an axis.
        void rotateFaceX(int faceNum, float angles);
        void rotateFaceY(int faceNum, float angles);
        void rotateFaceZ(int faceNum, float angles);

    public:
        GLRubiksCube(GLuint program);
        void allocate();
        void draw();
        bool isRotating();

        // Method for rotating a face by angles
        // (positive for clockwise, negative for counter clockwise)
        bool rotateFace(int faceNum, float angles);

    private:
        void initModelMatrices();
};

#endif