#include "obj.h"

using namespace std;


// Some colors.
const color4 red     = color4( 1.0, 0.0, 0.0, 1.0 );
const color4 yellow  = color4( 1.0, 1.0, 0.0, 1.0 );
const color4 green   = color4( 0.0, 1.0, 0.0, 1.0 );
const color4 blue    = color4( 0.0, 0.0, 1.0, 1.0 );
const color4 magenta = color4( 1.0, 0.0, 1.0, 1.0 );
const color4 cyan    = color4( 0.0, 1.0, 1.0, 1.0 );
const color4 grey    = color4( 0.3, 0.3, 0.3, 1.0 );

/*
    GLObjectState class implementation.
    Class is used for storing the rotation and scale information
    of an object. And used for generating the ModelView matrix for
    object in its current state.   
*/
const GLfloat GLObjectState::DEFAULT_X = 0.0;
const GLfloat GLObjectState::DEFAULT_Z = 0.0;
const GLfloat GLObjectState::DEFAULT_SCALE = 1.0;
const GLfloat GLObjectState::MIN_SCALE = 0.2;
const GLfloat GLObjectState::MAX_SCALE = 2.0;

// Custom constructor.
GLObjectState::GLObjectState(const GLfloat x_rot, const GLfloat z_rot, const GLfloat scale){
    this->init_x = x_rot;
    this->init_z = z_rot;
    this->init_scale = scale;
    this->rotateMatrix = RotateX(x_rot) * RotateZ(z_rot);
    this->scale = scale;
}

/*
    Rotate and zoom methods implemented straightforwardly.
*/
void GLObjectState::rotateX(const GLfloat rot){
    this->rotateMatrix = RotateX(rot) * this->rotateMatrix;
}

void GLObjectState::rotateZ(const GLfloat rot){
    this->rotateMatrix = RotateZ(rot) * this->rotateMatrix;
}

void GLObjectState::zoomIn(const GLfloat val){
    GLfloat s_val = scale + val;
    if(s_val <= MAX_SCALE)
        this->scale = s_val;
}

void GLObjectState::zoomOut(const GLfloat val){
    GLfloat s_val = scale - val;
    if(s_val >= MIN_SCALE)
        this->scale = s_val;
}

// reset method for reseting the object to its defualt position.
void GLObjectState::reset(){
    this->resetRotation();
    this->scale = this->init_scale;
}

// resetRotation method resets only rotation, leaving scale constant.
void GLObjectState::resetRotation(){
    this->rotateMatrix = RotateX(this->init_x) * RotateZ(this->init_z);
}


mat4 GLObjectState::getModelViewMatrix(){
    return Scale(this->scale, this->scale, this->scale) * this->rotateMatrix;
}


/*
    abstract GLObject class and its subclasses.
    Used for capsulating a objects properties,
    allocating memory from gpu and drawing.
*/

/* Static shader related members */
const char* GLObject::VERTEX_POSITION_ATTRIB = "vPosition";
const char* GLObject::VERTEX_COLOR_ATTRIB = "vColor";
const char* GLObject::MODEL_MATRIX_ATTRIB = "ModelMatrix";


GLObject::GLObject(GLuint program){
    this->program = program;
}


/*
    A simple unit cube implementation, placed in the origin.
    Vertices and indices are defualt member variables defined in header file.
    Just implemeted in order to debug easier.
    Uses element array buffer to draw the cube.
*/
GLCube::GLCube(GLuint program): GLObject(program){
    this->vao_id = 0;
    this->vbo_ids[0] = 0;
    this->vbo_ids[1] = 0;
    this->positionAttrib = glGetAttribLocation(program, VERTEX_POSITION_ATTRIB);
    this->colorAttrib = glGetAttribLocation(program, VERTEX_COLOR_ATTRIB);
}

void GLCube::allocate(){
    // a dummy color for vertices.
    color4 colors[8];
    color4 dummy_color = color4(1.0, 1.0, 1.0, 1.0);

    int i;
    for(i=0; i < 8; i++){
        colors[i] = dummy_color;
    }


    glUseProgram(program);
    // Create an initilize vertex array object
    if(vao_id == 0)
        glGenVertexArrays(1, &vao_id);

    glBindVertexArray(vao_id);
    // Create and initialize a buffer object;
    // First index for vertex objects, second for index objects.
    if(vbo_ids[0] == 0 && vbo_ids[1] == 0)
        glGenBuffers(2, &vbo_ids[0]);

    glBindBuffer(GL_ARRAY_BUFFER, vbo_ids[0]);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices) + sizeof(colors), NULL, GL_STATIC_DRAW);
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertices), vertices);
    glBufferSubData(GL_ARRAY_BUFFER, sizeof(vertices), sizeof(colors), colors);

    // set up vertex arrays;
    glEnableVertexAttribArray(this->positionAttrib);
    glVertexAttribPointer(this->positionAttrib, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));

    glEnableVertexAttribArray(this->colorAttrib);
    glVertexAttribPointer(this->colorAttrib, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(sizeof(vertices)));

    // set up index buffer objects.
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo_ids[1]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

    glBindVertexArray(0);
    glUseProgram(0);
}


void GLCube::draw(){
    if(vao_id != 0){
        glUseProgram(program);
        glBindVertexArray(vao_id);

        glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, (GLvoid*)0);

        glBindVertexArray(0);
        glUseProgram(0);
    }
}

/*
    A colored cube. Each face off the cube has a selectable color.
    Each vertex is stored duplicately in order to work around the iterpolation
    in fragment shader. Hence no index buffer is used.

    For the cube face 6 consecutive integers (starting from 0 and increasing) are used.
        FRONT:  0
        BOTTOM: 1
        LEFT:   2
        RIGHT:  3
        TOP:    4
        BACK:   5

*/

// Default colors for the cube.
const color4 GLColorCube::DEFAULT_FACE_COLORS[6] = {red , green, magenta, yellow, blue, cyan};

GLColorCube::GLColorCube(GLuint program, color4 colors[6]) : GLObject(program) {
    this->vao_id = 0;
    this->vbo_id = 0;
    this->positionAttrib = glGetAttribLocation(program, VERTEX_POSITION_ATTRIB);
    this->colorAttrib = glGetAttribLocation(program, VERTEX_COLOR_ATTRIB);

    int i;
    if (colors == nullptr){
        for(i = 0; i < 6; i++)
            this->faceColors[i] = DEFAULT_FACE_COLORS[i];
    } else {
        for(i = 0; i < 6; i++)
            this->faceColors[i] = colors[i];
    }
}

void GLColorCube::allocate(){
    point4 vertexArray[36];
    color4 vertexColorArray[36];

    int i;
    for(i = 0; i < 36; i++){
        vertexArray[i] = this->vertices[this->indices[i]];
        vertexColorArray[i] = this->faceColors[i/6];
    } 

    glUseProgram(this->program);
    // Create an initilize vertex array object
    if(this->vao_id == 0)
        glGenVertexArrays(1, &(this->vao_id));
    
    glBindVertexArray(this->vao_id);

    if(this->vbo_id == 0)
        glGenBuffers(1, &(this->vbo_id));

    glBindBuffer(GL_ARRAY_BUFFER, this->vbo_id);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertexArray) + sizeof(vertexColorArray), NULL, GL_STATIC_DRAW );
    glBufferSubData(GL_ARRAY_BUFFER, 0, sizeof(vertexArray), vertexArray);
    glBufferSubData(GL_ARRAY_BUFFER, sizeof(vertexArray), sizeof(vertexColorArray), vertexColorArray);

    // set up attributes;
    glEnableVertexAttribArray(this->positionAttrib);
    glVertexAttribPointer(this->positionAttrib, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(0));
    glEnableVertexAttribArray(this->colorAttrib);
    glVertexAttribPointer(this->colorAttrib, 4, GL_FLOAT, GL_FALSE, 0, BUFFER_OFFSET(sizeof(vertexArray)));

    glBindVertexArray(0);
    glUseProgram(0);

}

void GLColorCube::draw(){
    if(this->vao_id != 0){
        glUseProgram(program);
        glBindVertexArray(this->vao_id);

        glDrawArrays(GL_TRIANGLES, 0, 36);

        glBindVertexArray(0);
        glUseProgram(0);
    }
}

/*
    Extendion of a GLColorCube. Have a select method used for retuning a
    the face number. And have a modelMatrix to make it drawable by itself. 
*/
GLSelectionCube::GLSelectionCube(GLuint program) : GLColorCube(program, NULL) {
    this->modelMatrixLocation = glGetUniformLocation(program, MODEL_MATRIX_ATTRIB);
    this->modelMatrix = mat4();
}

void GLSelectionCube::draw(){
    if(this->vao_id != 0){
        glUseProgram(program);
        glUniformMatrix4fv(this->modelMatrixLocation, 1, GL_TRUE, this->modelMatrix);
        GLColorCube::draw();
        glUseProgram(0);
    }
}

/*
    Returns the face index corresponding to x, y on the frame buffer.
*/
int GLSelectionCube::select(int x, int y){
    unsigned char pixel[4];
    glReadPixels(x, y, 1, 1, GL_RGB, GL_UNSIGNED_BYTE, pixel);

    int pixel_r = (int)pixel[0];
    int pixel_g = (int)pixel[1];
    int pixel_b = (int)pixel[2];

    int i;
    for(i = 0; i < 6; i++){
        color4 faceColor = faceColors[i];
        int face_r = (int) (faceColor.x * 255.0);
        int face_g = (int) (faceColor.y * 255.0);
        int face_b = (int) (faceColor.z * 255.0);
        if(face_r == pixel_r && face_g == pixel_g && face_b == pixel_b){
            // Return face index
            return i;
        }
    }

    // no match.
    return -1;
}


GLRubiksCube::GLRubiksCube(GLuint program): GLObject(program){
    // Clear the subCubes array
    int i;
    for(i = 0; i < GLRubiksCube::NUM_SUBCUBES; i++){
        this->subCubes[i] = nullptr;
    }
    
    this->modelMatrixLocation = glGetUniformLocation(program, MODEL_MATRIX_ATTRIB);

    // Color to use for inside of the sub cubes.
    color4 c_in = color4(0.0, 0.0, 0.0, 1.0);

    // There are many initializations below that can be confusing.
    // All off them were calculated on pen and paper, then written here.

    // Colors for each subcube.
    color4 subCubeColors[NUM_SUBCUBES][6] = {
        {red , c_in  , green, c_in, magenta, c_in},
        {red , c_in  , c_in , c_in, magenta, c_in},
        {red , c_in  , c_in , blue, magenta, c_in},
        {red , c_in  , green, c_in, c_in   , c_in},
        {red , c_in  , c_in , c_in, c_in   , c_in},
        {red , c_in  , c_in , blue, c_in   , c_in},
        {red , yellow, green, c_in, c_in   , c_in},
        {red , yellow, c_in , c_in, c_in   , c_in},
        {red , yellow, c_in , blue, c_in   , c_in},
        {c_in, c_in  , green, c_in, magenta, c_in},
        {c_in, c_in  , c_in , c_in, magenta, c_in},
        {c_in, c_in  , c_in , blue, magenta, c_in},
        {c_in, c_in  , green, c_in, c_in   , c_in},
        {c_in, c_in  , c_in , blue, c_in   , c_in},
        {c_in, yellow, green, c_in, c_in   , c_in},
        {c_in, yellow, c_in , c_in, c_in   , c_in},
        {c_in, yellow, c_in , blue, c_in   , c_in},
        {c_in, c_in  , green, c_in, magenta, cyan},
        {c_in, c_in  , c_in , c_in, magenta, cyan},
        {c_in, c_in  , c_in , blue, magenta, cyan},
        {c_in, c_in  , green, c_in, c_in   , cyan},
        {c_in, c_in  , c_in , c_in, c_in   , cyan},
        {c_in, c_in  , c_in , blue, c_in   , cyan},
        {c_in, yellow, green, c_in, c_in   , cyan},
        {c_in, yellow, c_in , c_in, c_in   , cyan},
        {c_in, yellow, c_in , blue, c_in   , cyan}
    };

    for(i=0; i < NUM_SUBCUBES; i++)
        this->subCubes[i] = new GLColorCube(program, subCubeColors[i]);

    // Positions of teach subcube relative to the center of the rubik cube in rubik cubes space.
    this->subCubePositions[0]  = point3( -1.0 ,  1.0 ,   1.0 ) * this->scaleDistance;
    this->subCubePositions[1]  = point3(  0.0 ,  1.0 ,   1.0 ) * this->scaleDistance;
    this->subCubePositions[2]  = point3(  1.0 ,  1.0 ,   1.0 ) * this->scaleDistance;
    this->subCubePositions[3]  = point3( -1.0 ,  0.0 ,   1.0 ) * this->scaleDistance;
    this->subCubePositions[4]  = point3(  0.0 ,  0.0 ,   1.0 ) * this->scaleDistance;
    this->subCubePositions[5]  = point3(  1.0 ,  0.0 ,   1.0 ) * this->scaleDistance;
    this->subCubePositions[6]  = point3( -1.0 , -1.0 ,   1.0 ) * this->scaleDistance;
    this->subCubePositions[7]  = point3(  0.0 , -1.0 ,   1.0 ) * this->scaleDistance;
    this->subCubePositions[8]  = point3(  1.0 , -1.0 ,   1.0 ) * this->scaleDistance;
    this->subCubePositions[9]  = point3( -1.0 ,  1.0 ,   0.0 ) * this->scaleDistance;
    this->subCubePositions[10] = point3(  0.0 ,  1.0 ,   0.0 ) * this->scaleDistance;
    this->subCubePositions[11] = point3(  1.0 ,  1.0 ,   0.0 ) * this->scaleDistance;
    this->subCubePositions[12] = point3( -1.0 ,  0.0 ,   0.0 ) * this->scaleDistance;
    this->subCubePositions[13] = point3(  1.0 ,  0.0 ,   0.0 ) * this->scaleDistance;
    this->subCubePositions[14] = point3( -1.0 , -1.0 ,   0.0 ) * this->scaleDistance;
    this->subCubePositions[15] = point3(  0.0 , -1.0 ,   0.0 ) * this->scaleDistance;
    this->subCubePositions[16] = point3(  1.0 , -1.0 ,   0.0 ) * this->scaleDistance;
    this->subCubePositions[17] = point3( -1.0 ,  1.0 ,  -1.0 ) * this->scaleDistance;
    this->subCubePositions[18] = point3(  0.0 ,  1.0 ,  -1.0 ) * this->scaleDistance;
    this->subCubePositions[19] = point3(  1.0 ,  1.0 ,  -1.0 ) * this->scaleDistance;
    this->subCubePositions[20] = point3( -1.0 ,  0.0 ,  -1.0 ) * this->scaleDistance;
    this->subCubePositions[21] = point3(  0.0 ,  0.0 ,  -1.0 ) * this->scaleDistance;
    this->subCubePositions[22] = point3(  1.0 ,  0.0 ,  -1.0 ) * this->scaleDistance;
    this->subCubePositions[23] = point3( -1.0 , -1.0 ,  -1.0 ) * this->scaleDistance;
    this->subCubePositions[24] = point3(  0.0 , -1.0 ,  -1.0 ) * this->scaleDistance;
    this->subCubePositions[25] = point3(  1.0 , -1.0 ,  -1.0 ) * this->scaleDistance;

    // Initialize model matrices.
    this->initModelMatrices();

    // Assign indices of subcubes on each face. (This remains constant, subcube indices changes on a complete rotation)
    // This is a mess. I modelled on pen and paper and written down here later.
    this->faces[0][0] =  0; this->faces[0][1] =  1; this->faces[0][2] =  2; this->faces[0][3] =  3; this->faces[0][4] =  4; this->faces[0][5] =  5; this->faces[0][6] =  6; this->faces[0][7] =  7; this->faces[0][8] =   8;
    this->faces[1][0] =  6; this->faces[1][1] =  7; this->faces[1][2] =  8; this->faces[1][3] = 14; this->faces[1][4] = 15; this->faces[1][5] = 16; this->faces[1][6] = 23; this->faces[1][7] = 24; this->faces[1][8] =  25;
    this->faces[2][0] = 17; this->faces[2][1] =  9; this->faces[2][2] =  0; this->faces[2][3] = 20; this->faces[2][4] = 12; this->faces[2][5] =  3; this->faces[2][6] = 23; this->faces[2][7] = 14; this->faces[2][8] =   6;
    this->faces[3][0] =  2; this->faces[3][1] = 11; this->faces[3][2] = 19; this->faces[3][3] =  5; this->faces[3][4] = 13; this->faces[3][5] = 22; this->faces[3][6] =  8; this->faces[3][7] = 16; this->faces[3][8] =  25;
    this->faces[4][0] = 17; this->faces[4][1] = 18; this->faces[4][2] = 19; this->faces[4][3] =  9; this->faces[4][4] = 10; this->faces[4][5] = 11; this->faces[4][6] =  0; this->faces[4][7] =  1; this->faces[4][8] =   2;
    this->faces[5][0] = 19; this->faces[5][1] = 18; this->faces[5][2] = 17; this->faces[5][3] = 22; this->faces[5][4] = 21; this->faces[5][5] = 20; this->faces[5][6] = 25; this->faces[5][7] = 24; this->faces[5][8] =  23;
}

/* Allocate each subcube.*/
void GLRubiksCube::allocate(){
    int i;
    GLColorCube *subCube_p;
    for(i = 0; i < NUM_SUBCUBES; i++){
        subCube_p = this->subCubes[i];
        if(subCube_p != nullptr)
            subCube_p->allocate();
    }
}

/* Set the corresponding modelMatrix and draw each subcube. */
void GLRubiksCube::draw(){
    int i;
    GLColorCube *subCube_p;
    for(i = 0; i < NUM_SUBCUBES; i++){
        subCube_p = this->subCubes[i];
        if(subCube_p != nullptr){
            glUseProgram(program);
            glUniformMatrix4fv(this->modelMatrixLocation, 1, GL_TRUE, this->subCubeModelMatrices[i]);
            subCube_p->draw();
            glUseProgram(0);
        }
    }
}

void GLRubiksCube::initModelMatrices(){
    mat4 scaleMatrix = Scale(this->scaleSize);
    int i;
    for(i = 0; i < NUM_SUBCUBES; i++)
        this->subCubeModelMatrices[i] = Translate(this->subCubePositions[i]) * scaleMatrix;
}

bool GLRubiksCube::isRotating(){
    if(this->rotatingFace >= 0)
        return true;
    else
        return false;
}

/*
    Updates rotatingFace and rotatedAngle state variables for starting rotation
    Returns true if the method is called on a valid state (not rotating)
    Returns false if method is called on a already rotating state or called with an invalid face index.
*/
bool GLRubiksCube::startRotation(int faceNum){
    if(!this->isRotating() && !(faceNum < 0 || faceNum > 5)){
        this->rotatingFace = faceNum;
        this->rotatedAngle = 0;
        return true;
    } else {
        return false;
    }
}

/*
    Rotates the given face by angles degrees. For positive angles rotation is clokwise.
    For negative angles rotation is counterclockwise.
    On a complete rotation comples the rotation (Operation where sub cube indices are rearranged.)

    Returns true is rotation was successful.
    Returns false when a rotation was unsuccessful. (Called on a invalid state or called with invalid parameters.)
    A unsuccessful rotation can occur on these conditions:
        - The rubik cube is in a rotating state and method is called with a faceNum other then the rotating face.
        - Method is called with a invalid faceNum.
*/
bool GLRubiksCube::rotateFace(int faceNum, float angles){
    if(this->isRotating()){
        if(faceNum != this->rotatingFace){
            return false;
        }
    } else {
        if(angles != 0.0f){
            bool startResult = this->startRotation(faceNum);
            if(!startResult)
                return false;
        } else {
            return true;
        }
    }

    if(angles >= 0){
        angles = min(90.0f - this->rotatedAngle, angles);
        this->rotatedAngle = min(90.0f, this->rotatedAngle + angles);
    } else {
        angles = max(-90.0f - this->rotatedAngle, angles);
        this->rotatedAngle = max(-90.0f, this->rotatedAngle + angles);
    }

    if(angles != 0.0f){
        switch(faceNum){
            case 0:
                this->rotateFaceZ(faceNum, -1.0 * angles);
                break;
            case 1:
                this->rotateFaceY(faceNum,  1.0 * angles);
                break;
            case 2:
                this->rotateFaceX(faceNum,  1.0 * angles);
                break;
            case 3:
                this->rotateFaceX(faceNum, -1.0 * angles);
                break;
            case 4:
                this->rotateFaceY(faceNum, -1.0 * angles);
                break;
            case 5:
                this->rotateFaceZ(faceNum,  1.0 * angles);
                break;
            default:
                return false;
        }
    }
    
    if(this->rotatedAngle >= 90.0f || this->rotatedAngle <= -90.0f){
        direction_t direction;
        if(angles > 0.0f){
            direction = direction_t::CW;
        } else {
            direction = direction_t::CCW;
        }
        this->completeRotation(faceNum, direction);
    }

    return true;
}

/* 
    A complete rotation occurence. Updates the subCube and subCubeModelMatrices indices
    to correspond with the sub cube indices stored in faces array.
*/
void GLRubiksCube::completeRotation(int faceNum, direction_t direction){
    GLColorCube * copyFaceCubes[9];
    mat4 copyFaceMatrices[9];
    int *face = this->faces[faceNum];
    
    // copy the cube.
    int i;
    for(i = 0; i < 9; i++){
        copyFaceCubes[i] = this->subCubes[face[i]];
        copyFaceMatrices[i] = this->subCubeModelMatrices[face[i]];
    }

    // replace indices at rotated face. These two maps are calculated on pen an paper and just works.
    if (direction == direction_t::CW){
        this->subCubes[face[0]] = copyFaceCubes[6]; this->subCubeModelMatrices[face[0]] = copyFaceMatrices[6];
        this->subCubes[face[1]] = copyFaceCubes[3]; this->subCubeModelMatrices[face[1]] = copyFaceMatrices[3];
        this->subCubes[face[2]] = copyFaceCubes[0]; this->subCubeModelMatrices[face[2]] = copyFaceMatrices[0];
        this->subCubes[face[3]] = copyFaceCubes[7]; this->subCubeModelMatrices[face[3]] = copyFaceMatrices[7];
        this->subCubes[face[4]] = copyFaceCubes[4]; this->subCubeModelMatrices[face[4]] = copyFaceMatrices[4];
        this->subCubes[face[5]] = copyFaceCubes[1]; this->subCubeModelMatrices[face[5]] = copyFaceMatrices[1];
        this->subCubes[face[6]] = copyFaceCubes[8]; this->subCubeModelMatrices[face[6]] = copyFaceMatrices[8];
        this->subCubes[face[7]] = copyFaceCubes[5]; this->subCubeModelMatrices[face[7]] = copyFaceMatrices[5];
        this->subCubes[face[8]] = copyFaceCubes[2]; this->subCubeModelMatrices[face[8]] = copyFaceMatrices[2];
    } else if (direction == direction_t::CCW){
        this->subCubes[face[0]] = copyFaceCubes[2]; this->subCubeModelMatrices[face[0]] = copyFaceMatrices[2];
        this->subCubes[face[1]] = copyFaceCubes[5]; this->subCubeModelMatrices[face[1]] = copyFaceMatrices[5];
        this->subCubes[face[2]] = copyFaceCubes[8]; this->subCubeModelMatrices[face[2]] = copyFaceMatrices[8];
        this->subCubes[face[3]] = copyFaceCubes[1]; this->subCubeModelMatrices[face[3]] = copyFaceMatrices[1];
        this->subCubes[face[4]] = copyFaceCubes[4]; this->subCubeModelMatrices[face[4]] = copyFaceMatrices[4];
        this->subCubes[face[5]] = copyFaceCubes[7]; this->subCubeModelMatrices[face[5]] = copyFaceMatrices[7];
        this->subCubes[face[6]] = copyFaceCubes[0]; this->subCubeModelMatrices[face[6]] = copyFaceMatrices[0];
        this->subCubes[face[7]] = copyFaceCubes[3]; this->subCubeModelMatrices[face[7]] = copyFaceMatrices[3];
        this->subCubes[face[8]] = copyFaceCubes[6]; this->subCubeModelMatrices[face[8]] = copyFaceMatrices[6];
    }

    // Update rotation state variables.
    this->rotatingFace = -1;
    this->rotatedAngle = 0;
}


/*
    Below are rotation methods of a face on a single dimention.
    They simply multiply the existing modelMatrix of subCubes on the given face
    with a rotation matrix.
*/

void GLRubiksCube::rotateFaceX(int faceNum, float angles){
    int *face = this->faces[faceNum];
    int i;
    int subIndex;
    point3 subPosition;
    for(i = 0; i < 9; i++){
        subIndex = face[i];
        subPosition = this->subCubePositions[subIndex];
        this->subCubeModelMatrices[subIndex] = RotateX(angles) * this->subCubeModelMatrices[subIndex];
    }
}

void GLRubiksCube::rotateFaceY(int faceNum, float angles){
    int *face = this->faces[faceNum];
    int i;
    int subIndex;
    point3 subPosition;
    for(i = 0; i < 9; i++){
        subIndex = face[i];
        subPosition = this->subCubePositions[subIndex];
        this->subCubeModelMatrices[subIndex] = RotateY(angles) * this->subCubeModelMatrices[subIndex];
    }
}

void GLRubiksCube::rotateFaceZ(int faceNum, float angles){
    int *face = this->faces[faceNum];
    int i;
    int subIndex;
    point3 subPosition;
    for(i = 0; i < 9; i++){
        subIndex = face[i];
        subPosition = this->subCubePositions[subIndex];
        this->subCubeModelMatrices[subIndex] = RotateZ(angles) * this->subCubeModelMatrices[subIndex];
    }
}

// For some reason deleting this line causes segfault. Can't figure why.
int silly_segfault_workaround_variable = stoi("1");
