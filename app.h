#ifndef __APP_H
#define __APP_H

#include <string>
#include <queue>
#include <Angel.h>
#include "obj.h"

using namespace std;


void appInit(int *, char **);
void appStart();

typedef GLRubiksCube::direction_t direction_t;

struct rotation_t {
    int face;
    direction_t direction;
};

class ApplicationWindow {
    protected:
        // Shader program related static variables.
        static const char* VERTEX_SHADER;
        static const char* FRAGMENT_SHADER;
        static const char* MODELVIEW_MATRIX_NAME;
        static const char* PROJECTION_MATRIX_NAME;

        // Configuration for rotation animation smoothness.
        static const float ROTATION_ANGLE_PRECISION;
        static const float DEFAULT_ROTATION_PERIOD;
        static const float SHUFFLE_PERIOD;
        
        float rotationPeriod = DEFAULT_ROTATION_PERIOD; // Time it takes to complete a rotation.
        float beforeShufflePeriod = rotationPeriod;

        // Window related variables.
        string windowTitle;
        int width, height;
        int windowHandle;
        GLuint program;

        // uniform variable locations
        GLuint modelViewMarixLocation;
        GLuint projectionMatrixLocation;

        // trasformation matrices
        mat4 modelViewMatrix;
        mat4 projectionMatrix;

        bool selectionOpen; // State for selection.
        bool cubeRotating; // State for a face rotation.
        rotation_t selectedRotation; // Currently rotating face.
        bool shuffling = false;
        queue<rotation_t> rotationQueue;

        // Variable storing the object state.
        GLObjectState * o_state = new GLObjectState(-45.0, -45.0); // object state contains the rotation and zoom values for both objects.
        GLRubiksCube * c_object = nullptr; // current object on display
        GLSelectionCube * selectionObject = nullptr; // selection object on display

        // Methods for updating matrices.
        void updateModelView();
        void updateProjection();

        void installSelection(); // Attaches the selection listener and starts selection.
        void processSelection(int button, int x, int y); // Processes the incoming selection at the coordinations.
        void blockSelection(); // Blocks users ability to select from screen.
        void unblockSelection(); // Removes selection block.


        // Rubicks cube rotation handling methods.
        void addRotation(rotation_t rotation); // Adds a face rotation to queue.
        void startRotation(rotation_t rotation); // Adds a face rotation and starts it.
        void startRotation(); // Starts processing rotations in the queue.
        void continueRotation(); // Method used for processing partial rotations to complete into a whole rotations.
        void rotationStep(); // This method processes a partial rotation.
        void endRotation(); // Method used for alerting that the rotation processes has finished.

        void setRotationPeriod(float period);

    public:
        ApplicationWindow(const string& w_title, const int& w_height, const int& w_width);
        
        void shuffleCube(unsigned int numRotations);

        // Methods used for rendering and processing events from mouse and keyboard.
        void render();
        void resize(int width, int height);
        void keyControl(unsigned char key, int x, int y);
        void specialKeyControl(int key, int x, int y);
        void mouseControl(int button, int state, int x, int y);
        void rotationTimer();
        
};
#endif
