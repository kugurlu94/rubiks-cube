#include <iostream>
#include <Angel.h>
#include <random>
#include "app.h"

using namespace std;

// current ApplicationWindow on the focus.
// used for obtaining callback registrations.
// This works as a singleton, and used for
// adding object-orientism to C OpenGL API.
ApplicationWindow* c_appWindow;

const string WINDOW_TITLE = "Rubik's Cube";

// Default window sizes.
const int 
    APP_WINDOW_WIDTH = 900,
    APP_WINDOW_HEIGHT = 900;

// Default count shuffle
const int SHUFFLE_COUNT = 10;

// Amount of rotation for each key event.
const GLfloat ROTATION_DELTA = 3.0;


void appInit(int* argc, char* argv[]){
    // Initilizes the OpenGL context for the application
    // Global glut options.
    glutInit(argc, argv);
    glutInitContextVersion(4, 5); // I used this version in my system.
    glutInitContextFlags(GLUT_FORWARD_COMPATIBLE);
    glutInitContextProfile(GLUT_CORE_PROFILE);
    glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
    glutSetOption(
        GLUT_ACTION_ON_WINDOW_CLOSE,
        GLUT_ACTION_GLUTMAINLOOP_RETURNS
    );
}

/*
    Callback functions to attach for events.
*/

void displayCallback(){
    if(c_appWindow != nullptr)
        c_appWindow->render();
}

void resizeCallback(int width, int height){
    if(c_appWindow != nullptr)
        c_appWindow->resize(width, height);
}

void keyboardCallback(unsigned char key, int x, int y){
    if(c_appWindow != nullptr)
        c_appWindow->keyControl(key, x, y);
}

void specialCallBack(int key, int x, int y){
    if(c_appWindow != nullptr)
        c_appWindow->specialKeyControl(key, x, y);
}

void mouseCallback(int button, int state, int x, int y){
    if(c_appWindow != nullptr)
        c_appWindow->mouseControl(button, state, x,  y);
}

void rotateTimerCallBack(int value){
    if(c_appWindow != nullptr)
        c_appWindow->rotationTimer();
}


void attachCallbacks(){
    glutDisplayFunc(displayCallback);
    glutReshapeFunc(resizeCallback);
    glutKeyboardFunc(keyboardCallback);
    glutSpecialFunc(specialCallBack);
    glutMouseFunc(mouseCallback);
}


void appStart(){
    c_appWindow = new ApplicationWindow(WINDOW_TITLE, APP_WINDOW_WIDTH, APP_WINDOW_HEIGHT);
    attachCallbacks();

    c_appWindow->shuffleCube(10);

    // Starts the main event loop.
    cout << "Welcome to Rubik's Cube!" << endl;
    cout << "Please press h for instructions." << endl;
    glutMainLoop();
    cout << "Bye..." << endl;
}

/* Static member variables */
const char* ApplicationWindow::VERTEX_SHADER =  "vshader.glsl";
const char* ApplicationWindow::FRAGMENT_SHADER = "fshader.glsl";
const char* ApplicationWindow::MODELVIEW_MATRIX_NAME = "ModelViewMatrix";
const char* ApplicationWindow::PROJECTION_MATRIX_NAME = "ProjectionMatrix";
const float ApplicationWindow::ROTATION_ANGLE_PRECISION = 1.0f;
const float ApplicationWindow::DEFAULT_ROTATION_PERIOD = 0.4f;
const float ApplicationWindow::SHUFFLE_PERIOD = 0.1f;

ApplicationWindow::ApplicationWindow(const string& w_title, const int& w_height, const int& w_width){
    /* Initilizes the application window */
    windowTitle = w_title;
    height = w_height;
    width = w_width;

    glutInitWindowSize(width, height);
    windowHandle = glutCreateWindow(windowTitle.c_str());
    if(windowHandle < 1) {
        cerr << "Cannot create window." << endl;
        exit(EXIT_FAILURE);
    }

    glewExperimental = GL_TRUE;
    GLenum GlewInitResult = glewInit();

    if (GLEW_OK != GlewInitResult) {
        cerr << "ERROR: " << glewGetErrorString(GlewInitResult) << endl;
        exit(EXIT_FAILURE);
    }
    glGetError(); // glew creates a error for some reason.

    // OpenGL configurations.
    glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);
    glFrontFace(GL_CCW);

    // Compiling shaders and generating program
    this->program = InitShader(VERTEX_SHADER, FRAGMENT_SHADER);

    // Get uniform variable locations
    glUseProgram(program);
    modelViewMarixLocation = glGetUniformLocation(program, MODELVIEW_MATRIX_NAME);
    projectionMatrixLocation = glGetUniformLocation(program, PROJECTION_MATRIX_NAME);
    glUseProgram(0);

    // Set Uniform matrices
    this->updateModelView();
    this->updateProjection();

    // Create object and allocate on gpu
    this->c_object = new GLRubiksCube(program);
    this->c_object->allocate();

    // Install and attach selection
    this->installSelection();

    glutPostRedisplay();
}

/*
    Shuffles the cube by numRotations random rotations
    Method generates random rotations and pushes to rotationQueue
    and calls startRotation. 
    Before shuffle rotation period is decreased, it's recovered at the completeRotation.
*/
void ApplicationWindow::shuffleCube(unsigned int numRotations){
    if(numRotations > 0){
        this->blockSelection(); // Done allow user to interact while adding rotations to queue.
        default_random_engine faceGen;
        uniform_int_distribution<int> faceDist(0, 5);
        uniform_int_distribution<int> dirDist(0, 1);

        this->shuffling = true;
        this->beforeShufflePeriod = this->rotationPeriod;
        this->setRotationPeriod(SHUFFLE_PERIOD);
        unsigned int i;
        for(i = 0; i < numRotations; i++){
            rotation_t rotation;
            rotation.face = faceDist(faceGen);
            rotation.direction = dirDist(faceGen) ? direction_t::CW : direction_t::CCW;
            this->addRotation(rotation); // add rotations.
        }
        this->startRotation();
    }
}

/*
    Creates the selection cube.
*/

void ApplicationWindow::installSelection(){
    this->selectionObject = new GLSelectionCube(program);
    this->selectionObject->allocate();
    this->selectionOpen = true;
}

/*
    Processes a selection attempt.
    Draws the selectionCube and calls select.
    If a valid face is selected respective rotations is started.
*/
void ApplicationWindow::processSelection(int button, int x, int y){
    if(this->selectionOpen){
        if(this->selectionObject != nullptr){
            glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
            
            // Fill back buffer
            selectionObject->draw();
            glFlush();

            // Picking
            int face = selectionObject->select(x, y);
            if(face >= 0){
                rotation_t rotation;
                rotation.face = face;
                if (button == GLUT_LEFT_BUTTON){
                    rotation.direction = direction_t::CW;
                } else if (button == GLUT_RIGHT_BUTTON){
                    rotation.direction = direction_t::CCW;
                } else {
                    return;
                }

                this->startRotation(rotation);
            }

            glutPostRedisplay();
        }
    }
}

void ApplicationWindow::blockSelection(){
    this->selectionOpen = false;
}

void ApplicationWindow::unblockSelection(){
    this->selectionOpen = true;
}

/* Adds a rotation to rotation queue */
void ApplicationWindow::addRotation(rotation_t rotation){
    this->rotationQueue.push(rotation);
}

/*
    Adds a rotation to rotation queue and starts rotation
    Should be called for a immediate rotation.
*/
void ApplicationWindow::startRotation(rotation_t rotation){
    this->addRotation(rotation);
    this->startRotation();
}

/*
    Starts processing rotations in the rotation queue.
    During the processing selection is blocks. (Recovered at endRotation())
*/
void ApplicationWindow::startRotation(){
    if(!this->rotationQueue.empty()){
        this->blockSelection();

        rotation_t rotation = this->rotationQueue.front();
        this->selectedRotation = rotation;
        this->rotationQueue.pop();
        this->cubeRotating = true;
        this->continueRotation();
    }
}

/*
    Schedules a partial rotation. 
*/
void ApplicationWindow::continueRotation(){
    glutTimerFunc((int) ((ROTATION_ANGLE_PRECISION / 90.0) * this->rotationPeriod * 1000), rotateTimerCallBack, 0);
}

/*
    Processes a partial rotation. The rotation state is hold on member variables.
    This method checks them and rotates the respective face on a cube.
    Then chooses one of three.
        - Finish the rotation (a complete rotation occured and not more rotations on the queue.)
        - Continue on next rotation (a complete rotation occured but there are more rotations on the queue.)
        - Continue on same rotation (a partial rotation occured but still needs more steps the complete the rotation.)
*/
void ApplicationWindow::rotationStep(){
    if(this->c_object != nullptr && this->cubeRotating){
        int face = this->selectedRotation.face;
        direction_t direction = this->selectedRotation.direction;
        bool rotateSuccess;
        if(direction == direction_t::CW)
            rotateSuccess = this->c_object->rotateFace(face, ROTATION_ANGLE_PRECISION);
        else
            rotateSuccess = this->c_object->rotateFace(face, -1.0f * ROTATION_ANGLE_PRECISION);

        if(!rotateSuccess){
            cout << "Invalid state for rotation!" << endl;
        }
        this->cubeRotating = this->c_object->isRotating();
        if(this->cubeRotating){
            this->continueRotation();
        }
        else{
            if(!this->rotationQueue.empty()){
                // More rotations exists.
                this->startRotation();
            } else {
                // Last rotation
                this->endRotation();
            }
        }

        glutPostRedisplay();
    }
}

/* 
    Called for signalling the end of all rotations in the queue.
    Unblocks the selection. If the start was called by shuffling, rotationPeriod before
    shuffle is recovered.
*/
void ApplicationWindow::endRotation(){
    this->cubeRotating = false;
    if(this->shuffling){
        this->shuffling = false;
        this->setRotationPeriod(this->beforeShufflePeriod);
    }

    this->unblockSelection();
}

void ApplicationWindow::setRotationPeriod(float period){
    this->rotationPeriod = period;
}


// Must be called on each view change(object state change.)
void ApplicationWindow::updateModelView(){
    // Reload modelview matrice.
    glUseProgram(program);
    this->modelViewMatrix = this->o_state->getModelViewMatrix();
    glUniformMatrix4fv(this->modelViewMarixLocation, 1, GL_TRUE, this->modelViewMatrix);
    glUseProgram(0);

    glutPostRedisplay();
}


// Renders the current object on the focus.
void ApplicationWindow::render(){
    // The rendering function.
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Any drawing should be done here
    if (c_object != nullptr){
        c_object->draw();
    }

    glutSwapBuffers();
}

// Resizes viewport on window size changes. Updates projection matrix.
void ApplicationWindow::resize(int width, int height){
    this->width = width;
    this->height = height;
    glViewport(0, 0, width, height);
    this->updateProjection();

}

/* Update projection matrix with current width and height */
void ApplicationWindow::updateProjection(){
    if (this->width <= this->height)
        this->projectionMatrix = Ortho(-1.0, 1.0, -1.0 * (GLfloat) this->height / (GLfloat) this->width,
                           1.0 * (GLfloat) this->height / (GLfloat) this->width, -1.0, 1.0);
    else
        this->projectionMatrix = Ortho(-1.0* (GLfloat) this->width / (GLfloat) this->height, 1.0 * 
                             (GLfloat) this->width / (GLfloat) this->height, -1.0, 1.0, -1.0, 1.0);

    glUseProgram(this->program);
    glUniformMatrix4fv(this->projectionMatrixLocation, 1, GL_TRUE, this->projectionMatrix);
    glUseProgram(0);
}


// Arrow controls.
void ApplicationWindow::specialKeyControl(int key, int x, int y){
    switch (key){
        case GLUT_KEY_LEFT:
        {
            this->o_state->rotateZ(ROTATION_DELTA);
            this->updateModelView();
            break;
        }
        case GLUT_KEY_RIGHT:
        {
            this->o_state->rotateZ(-1.0 * ROTATION_DELTA);
            this->updateModelView();
            break;
        }
        case GLUT_KEY_DOWN:
        {
            this->o_state->rotateX(ROTATION_DELTA);
            this->updateModelView();
            break;
        }
        case GLUT_KEY_UP:
        {
            this->o_state->rotateX(-1.0 * ROTATION_DELTA);
            this->updateModelView();
            break;
        }
        default:
        {
            break;
        }
    }
}

// Keyboard controls.
void ApplicationWindow::keyControl(unsigned char key, int x, int y){
    switch (key)
    {
        case 'i':
            {
                this->o_state->resetRotation();
                this->updateModelView();
                break;
            }
        case 'h':
            {
                cout << endl << "### HELP ###" << endl
                    << "Arrow keys -- set the object rotation around X-axis (up and down arrow) and around Z-axis (left and right arrow)." << endl
                    << "i -- initialize rotation." << endl
                    << "Click on a face to rotate that face." << endl
                    << "Left click -- rotate clockwise." << endl
                    << "Right click -- rotate counter clockwise." << endl
                    << "s -- shuffle cube." << endl
                    << "h -- help, print explanation of your input commands." << endl
                    << "q -- quit (exit) the program." << endl;
                break;
            }
        case 's':
            {
                if(!this->cubeRotating)
                    this->shuffleCube(SHUFFLE_COUNT);
                break;
            }
        case 'q':
            {
                glutLeaveMainLoop();
                break;
            }
        default:
            {
                break;
            }
    }
}

// Mouse controls
void ApplicationWindow::mouseControl(int button, int state, int x, int y){
    if (state == GLUT_DOWN){
        int gl_y = glutGet(GLUT_WINDOW_HEIGHT) - y;
        this->processSelection(button, x, gl_y);
    }
}

/* Called on a timer alert */
void ApplicationWindow::rotationTimer(){
    this->rotationStep();
}