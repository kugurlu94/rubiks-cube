#version 450

in  vec4 vPosition;
in  vec4 vColor;
out vec4 color;

uniform mat4 ModelMatrix;
uniform mat4 ModelViewMatrix;
uniform mat4 ProjectionMatrix;

void main() 
{
    gl_Position = ProjectionMatrix * ModelViewMatrix * ModelMatrix * vPosition;
    color = vColor;
} 
